# deploy_kubernetes_platform playbook

## Purpose

Install tools to monitor the RKE2 Kubernetes cluster

## Roles

- orange.k8s_platform.external_requirements
- orange.k8s_platform.longhorn
- orange.k8s_platform.prometheus
- orange.k8s_platform.elasticsearch
- orange.k8s_platform.vault
- orange.k8s_platform.fluentd

## Mandatory parameters

TBD
