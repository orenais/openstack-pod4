# install_kubernetes playbook

## Purpose

Install a RKE2 Kubernetes cluster on top of hardened bare metal servers

## Roles

- orange.rke2.prepare
- orange.rke2.external_requirements
- orange.rke2.configure
- orange.rke2.deploy
- orange.rke2.postconfigure

## Mandatory parameters

TBD
