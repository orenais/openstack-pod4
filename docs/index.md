# OPENSTACK-POD4 deployment project

## Purpose

The aim of this deployment project is to install ORONAP on target
infrastructure.

## structure

```shell
.
├── .ssh
├── docs
├── inventory
│   └── group_vars
│   └── host_vars
│   └── inventory files
├── playbooks
└── vars
└── requirement files
└── CI files
```

### .ssh

A `.ssh` directory with a config file describes all the ssh configuration to
access the servers and vms.  

### docs

This directory contains the documentation files related to this deployment
chain. It includes the documentation of the playbooks used to execute the full
chain as well as the description of the needed override files to specify a
target environement. It does not include the detailed documentation of the
collection.

### inventory

#### Inventory file

The inventory must be named `oronap_$TARGET` with `$TARGET` equal to the
environment of your choice you wish to deploy ORONAP on.

The inventory must only provide a jumphost group

#### Variable files in inventory

Playbooks launch ansible collections and variables used by these collections can
be overwritten.

To perform this, group_vars and host_vars should be correctly set.

##### group_vars

`all.yml`: contains a link to `vars/oronap_common_override.yml`
In this file, you will find the global property parameters to install ORONAP.

`$TARGET.yml`: contains links to files in vars directory

- `vars/oronap_$TARGET_override.yml`: the property file

- `vars/oronap_$TARGET_servers.yml` : the infrastructure file

You can use both files to override default parameters to install ORONAP in your
environment.

Keep in mind that most playbooks use the property file.
If you plan to install several ORONAP platforms in your data center, the
use of `vars/oronap_common_override.yml` is recommended for common parameters.

ORONAP version as well as the list of ORONAP components are set in `vars/oronap_$TARGET_override.yml`
For example, depending if you plan to use a pikeo (community SDNC controller)
or lighty (lighty network controller) deployment.

- for Pikeo: `sdnc-components-overrides.yml` is used

- for Lighty: `lighty-components-overrides.yml` is used

##### host_vars

Only a jumphost

### Requirement Files

Requirements are yaml files with ORONAP collections versions. They allow you to
have different features included in your deployment.

### CI files

CI files define pipelines to be launched in order to install an ORONAP platform.

## Using Pipelines

### Mandatory parameters

Before using the project by launching a pipeline, you need to specify the
TARGET variable in the run pipeline variables

| Variable    | Purpose                         | Default value               |
|-------------|---------------------------------|-----------------------------|
| `TARGET`    | ORONAP target infrastructure    | `daily_pod4`                 |

Possible values are:

<!-- markdownlint-disable line-length -->
| TARGET           | Description                                       | Requirements                    |
|------------------|---------------------------------------------------|---------------------------------|
| `daily_pod4`     | Daily deployment of ORONAP Pikeo Master           | requirements_daily_pod4.yml     |
| `weekly_pod4`    | Weekly deployment of ORONAP Pikeo Master          | requirements_weekly_pod4.yml    |
| `innovlab_pod4`  | On demand deployment of ORONAP Pikeo Master       | requirements_innovlab_pod4.yml  |
<!-- markdownlint-enable line-length -->

Each target has its own requirements you can use to deploy different ORONAP
versions.

### Scheduled pipelines

| TARGET          | Scheduled at                                              |
|-----------------|-----------------------------------------------------------|
| `daily_pod4`    | Everyday at 1am                                           |
| `weekly_pod4`   | Every Sunday at 3am                                       |
| `innovlab_pod4` | On demand                                                 |

### Playbooks launched by pipelines

An ORONAP pipeline deals with the following stages:

<!-- markdownlint-disable line-length -->
| Stage    | Playbook launched          | Description                                                                           | Doc                                                |
|----------|----------------------------|---------------------------------------------------------------------------------------|----------------------------------------------------|
| clean    | clean                      | Clean environment                                                                     | [doc](./playbooks/clean.md)                        |
| infra    | create_vms                 | Deploy, harden and configure ORONAP vms                                               | [doc](./playbooks/create_vms.md)                   |
| caas     | install_kubernetes         | Deploy, harden and configure a kubernetes cluster on top of ORONAP bare metal servers | [doc](./playbooks/install_kubernetes.md)           |
| caas     | install_docker             | Install a Docker environment on top of ORONAP tests vm                                | [doc](./playbooks/install_docker.md)               |
| platform | deploy_kubernetes_platform | Deploy CaaS components to monitor, alert and log issues on the kubernetes cluster     | [doc](./playbooks/deploy_kubernetes_platform.md)   |
| onap     | install_onap               | Install ONAP application                                                              | [doc](./playbooks/install_onap.md)                 |
| test     | test_onap                  | Launch post installation tests to ckeck if ORONAP is up and running                   | [doc](./playbooks/test_onap.md)                    |
<!-- markdownlint-enable line-length -->
