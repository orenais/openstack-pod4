# ORONAP Deployments

Deploy ORONAP on Lannion-LB.

This project aims to deploy the following chains:

- daily: daily master of ORONAP with SDNC controller
- weekly: weekly master of ORONAP with SDNC controller
- innovlab: innovation lab for anticipation or research projects

Note the master version of ORONAP is always based on the last
release of ONAP.
