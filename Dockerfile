FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.11

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE
ARG USER=ansible
ARG DEPLOY_USER
ARG DEPLOY_PASSWORD
ARG TARGET

LABEL org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/onap/oronap/deployments/openstack-pod4/$TARGET" \
    org.opencontainers.image.created=$BUILD_DATE \
    org.opencontainers.image.ref.name="openstack-pod4/$TARGET" \
    org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com>" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

ENV HOME /home/$USER
ENV COLLECTION_PATH $HOME/.ansible/collections

# Push basic requirements
COPY requirements_$TARGET.yml /tmp/requirements.yml
WORKDIR $HOME/oronap_deployments

COPY inventory ./inventory/
COPY playbooks ./playbooks/
COPY vars ./vars/
COPY .ssh $HOME/.ssh/
COPY ansible.cfg /etc/ansible/

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# install galaxy deps
RUN mkdir -p "$COLLECTION_PATH" && \
    ansible-galaxy collection install -r /tmp/requirements.yml \
      -p "$COLLECTION_PATH" &&\
    # ADD the user
    chown $USER:$USER -R $HOME && chmod go-rwx -R $HOME

USER $USER
